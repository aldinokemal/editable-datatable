from django.db import models


# Create your models here.
class KategoriMakanan(models.Model):
    kategori_id = models.AutoField(primary_key=True)
    kategori_nama = models.CharField(max_length=255)
    kategori_asal = models.CharField(max_length=255)


class MenuMakanan(models.Model):
    menu_id = models.AutoField(primary_key=True)
    menu_name = models.CharField(max_length=255)
    menu_desc = models.CharField(max_length=255)
    menu_category = models.ForeignKey(KategoriMakanan, on_delete=models.CASCADE, verbose_name="the related poll")


class BahanMakanan(models.Model):
    bahan_id = models.AutoField(primary_key=True)
    bahan_name = models.CharField(max_length=255)
    bahan_satuan = models.CharField(max_length=255)


class ResepMakanan(models.Model):
    resep_id = models.AutoField(primary_key=True)
    resep_menu_id = models.ForeignKey(MenuMakanan, on_delete=models.CASCADE)
    resep_bahan_id = models.ForeignKey(BahanMakanan, on_delete=models.CASCADE)


class Karyawan(models.Model):
    karyawan_id = models.AutoField(primary_key=True)
    karyawan_nama = models.CharField(max_length=100)
    karyawan_alamat = models.CharField(max_length=255)
    karyawan_email = models.CharField(max_length=100)
    karyawan_nohp = models.CharField(max_length=100)
    karyawan_posisi = models.CharField(max_length=100, default='staff')
