from django.shortcuts import render
from django.http import JsonResponse
from django.core.mail import send_mail
# serialize object from model
from django.core import serializers
# csrf exception
from django.views.decorators.csrf import csrf_exempt

import re

# import models
from django.db.models import F
from .models import *

# debug
from django.http import HttpResponse


# import pdb; pdb.set_trace()


# Create your views here.
def sendEmail(request):
    sent = send_mail(
        'Subject here',
        'Here is the message.',
        'from@example.com',
        ['to@example.com'],
        fail_silently=False,
    )
    if sent:
        return JsonResponse({'succes': True, 'message': 'Email terkirim'})
    return JsonResponse({'succes': False, 'message': 'Terjadi kesalahan jaringan'})


# return render(request, 'send_email.html', {'krs' : 'wadaw'})

def getResep(request):
    menu = MenuMakanan.objects.annotate(menu_kategori=F('menu_category_id__kategori_nama'), ).values('menu_name',
                                                                                                     'menu_kategori',
                                                                                                     'menu_desc')
    return JsonResponse(list(menu), safe=False)


# sys.exit()


def showMenuResep(request):
    menu = MenuMakanan.objects.annotate(menu_kategori=F('menu_category_id__kategori_nama'), ).values('menu_name',
                                                                                                     'menu_kategori',
                                                                                                     'menu_desc')
    return render(request, 'resep.html', {'data': menu})


def showKaryawan(request):
    return render(request, 'karyawan.html');


def getKaryawan(request):
    karyawan = Karyawan.objects.all()
    # data = serializers.serialize("json", karyawan)

    data = []
    index = 1
    for x in karyawan:
        data.append({
            'DT_RowId': 'row_' + str(x.karyawan_id),
            'nama': x.karyawan_nama,
            'email': x.karyawan_email,
            'alamat': x.karyawan_alamat,
            'no_hp': x.karyawan_nohp,
            'posisi': x.karyawan_posisi
        })
        index += 1

    output = {
        'data': data,
        'options': [],
        'files': [],
    }
    return JsonResponse(output)


@csrf_exempt
def updKaryawan(request):
    if request.method == 'GET':
        return JsonResponse({'succes': False, 'error': 'Method NoT AlLoWeD!!!'})
    elif request.method == 'POST':
        print(request.POST)
        if request.POST['action'] == 'edit':
            index = 0
            for i in request.POST:
                if index > 0:
                    if ('email' in i):
                        dataID = re.findall(r'\[[\w0-9]+\]', i)[0].replace('[', '').replace(']', '').split('_')[1]
                        email = request.POST[i]
                        Karyawan.objects.filter(karyawan_id=int(dataID)).update(karyawan_email=email)
                    elif ('alamat' in i):
                        dataID = re.findall(r'\[[\w0-9]+\]', i)[0].replace('[', '').replace(']', '').split('_')[1]
                        alamat = request.POST[i]
                        Karyawan.objects.filter(karyawan_id=int(dataID)).update(karyawan_alamat=alamat)
                    elif ('nama' in i):
                        dataID = re.findall(r'\[[\w0-9]+\]', i)[0].replace('[', '').replace(']', '').split('_')[1]
                        nama = request.POST[i]
                        Karyawan.objects.filter(karyawan_id=int(dataID)).update(karyawan_nama=nama)
                    elif ('no_hp' in i):
                        dataID = re.findall(r'\[[\w0-9]+\]', i)[0].replace('[', '').replace(']', '').split('_')[1]
                        no_hp = request.POST[i]
                        Karyawan.objects.filter(karyawan_id=int(dataID)).update(karyawan_nohp=no_hp)
                    elif ('posisi' in i):
                        return JsonResponse({'succes': False, 'error': 'Data tidak bisa diupdate'})
                        # dataID = re.findall(r'\[[\w0-9]+\]', i)[0].replace('[', '').replace(']', '').split('_')[1]
                        # posisi = request.POST['data[row_{}][posisi]'.format(dataID)]
                        # Karyawan.objects.filter(karyawan_id=int(dataID)).update(karyawan_posisi=posisi)
                    else:
                        return JsonResponse({'succes': False, 'error': 'Terjadi kesalahan'})
                index += 1

        elif request.POST['action'] == 'create':
            karyawan_email = ''
            karyawan_alamat = ''
            karyawan_nama = ''
            karyawan_nohp = ''
            karyawan_posisi = ''
            for i in request.POST:
                if 'nama' in i:
                    karyawan_nama = request.POST[i]
                elif 'email' in i:
                    karyawan_email = request.POST[i]
                elif 'alamat' in i:
                    karyawan_alamat = request.POST[i]
                elif 'no_hp' in i:
                    karyawan_nohp = request.POST[i]
                elif 'posisi' in i:
                    karyawan_posisi = request.POST[i]

                # field = re.findall(r'\[[\w]+\]', i)[1].replace('[', '').replace(']', '')
                # if field == 'nama':
                #     karyawan_nama = request.POST['data[0][first_name]']
            insert = Karyawan.objects.create(karyawan_email=karyawan_email, karyawan_nama=karyawan_nama,
                                             karyawan_alamat=karyawan_alamat, karyawan_nohp=karyawan_nohp,
                                             karyawan_posisi=karyawan_posisi)
            insert.save()
            dataID = insert.karyawan_id

        elif request.POST['action'] == 'remove':
            for i in request.POST:
                if 'DT_RowId' in i:
                    row = request.POST[i]
                    dataID = row.split('_')[1]
                    Karyawan.objects.filter(karyawan_id=dataID).delete()
                    
        try:
            x = Karyawan.objects.get(karyawan_id=int(dataID))
            data = [
                {
                    'DT_RowId': 'row_' + str(x.karyawan_id),
                    'nama': x.karyawan_nama,
                    'email': x.karyawan_email,
                    'alamat': x.karyawan_alamat,
                    'no_hp': x.karyawan_nohp,
                    'posisi': x.karyawan_posisi
                }
            ]
        except Exception as e:
            data = []
            # return JsonResponse({'succes': False, 'error': 'Data tidak ditemukan'})

        output = {
            'data': data,
        }
        return JsonResponse(output)
