# Generated by Django 2.1.1 on 2018-09-26 03:12

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('inixindo', '0003_karyawan'),
    ]

    operations = [
        migrations.RenameField(
            model_name='karyawan',
            old_name='Karyawan_email',
            new_name='aryawan_nama',
        ),
        migrations.RenameField(
            model_name='karyawan',
            old_name='Karyawan_alamat',
            new_name='karyawan_alamat',
        ),
        migrations.RenameField(
            model_name='karyawan',
            old_name='Karyawan_nama',
            new_name='karyawan_email',
        ),
        migrations.RenameField(
            model_name='karyawan',
            old_name='Karyawan_nohp',
            new_name='karyawan_nohp',
        ),
    ]
